<?php
require_once __DIR__ . '/vendor/autoload.php';
$params = [
    'host' => '10.0.3.63',
    'port' => '502',
    'devAddr' => 1
];
    // init modbus obj;
    $modbus = new \Modbus\ModbusTCP(
            $params,
            new \Modbus\ModbusParser(),
            new \Modbus\PacketBuilder()
            );
    
    // write coil reg numb 3 value 64
    $modbus
            ->fc(Modbus\PacketBuilder::FC6)
            ->dataType([\Modbus\PacketBuilder::WORD])
            ->startReg(3)
            ->valReg(64);
    sendModbus($modbus);

    // read reg numb 4
    $modbus
            ->fc(Modbus\PacketBuilder::FC3)
            ->dataTYpe([\Modbus\PacketBuilder::FLOAT])
            ->startReg(4);
    sendModbus($modbus);
    // read 2 reg
    $modbus
            ->fc(Modbus\PacketBuilder::FC3)
            ->dataTYpe([\Modbus\PacketBuilder::WORD, \Modbus\PacketBuilder::WORD])
            ->startReg(0);
    sendModbus($modbus);

    
// send data
function sendModbus($modbus)
{
    try {
        $responseData = $modbus->send()->getResponse();
    } catch (Exception $exc) {
        $responseData = $exc->getMessage();
    }
    
    var_dump($responseData);
}