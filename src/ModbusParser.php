<?php
namespace Modbus;

use Modbus\Exceptions\ParserExceptions;
use Modbus\Interfaces\ParserInterface;
use Modbus\PacketBuilder;

class ModbusParser implements ParserInterface
{
    private $modbusResponse;
    private $answer;
    private $packetInfo;

    public function run($modbudResponse, PacketBuilder $packet) 
    {
        $this->modbusResponse = $modbudResponse;        
        $this->packetInfo = $packet;
        $this->checkError();
        $this->answer = [];
        
        switch (bin2hex($this->modbusResponse[7])) {
            case PacketBuilder::FC5;
            case PacketBuilder::FC6;
            case PacketBuilder::FC15;
            case PacketBuilder::FC16;
                $this->checkSuccessfullWrite();            
                break;
            case PacketBuilder::FC1;
            case PacketBuilder::FC2;
                $this->parsDIO();            
                break;
            case PacketBuilder::FC3;
            case PacketBuilder::FC4;
                $this->parsAIO(); 
                break;
            default:
                break;
        }        
          
        return $this->answer;
    }
    
    private function checkError() 
    {
        if ((ord($this->modbusResponse[7]) & 0x80) > 0) {
            $failure_code = ord($this->modbusResponse[8]);
            $failures = array(
                0x01 => "ILLEGAL FUNCTION",
                0x02 => "ILLEGAL DATA ADDRESS",
                0x03 => "ILLEGAL DATA VALUE",
                0x04 => "SLAVE DEVICE FAILURE",
                0x05 => "ACKNOWLEDGE",
                0x06 => "SLAVE DEVICE BUSY",
                0x08 => "MEMORY PARITY ERROR",
                0x0A => "GATEWAY PATH UNAVAILABLE",
                0x0B => "GATEWAY TARGET DEVICE FAILED TO RESPOND");
            // get failure string
            if (key_exists($failure_code, $failures)) {
                $failure_str = $failures[$failure_code];
            } else {
                $failure_str = "UNDEFINED FAILURE CODE";
            }            
            throw new ParserExceptions($failure_code);            
        }
    }
    
     private function checkSuccessfullWrite() 
    {
        $modbsResponseHex = bin2hex($this->modbusResponse);
        $sendDataHex = bin2hex($this->packetInfo->getPacket());
        $modbusFC = $this->packetInfo->getProperty("functionalCode");
        if($modbusFC == PacketBuilder::FC6 or $modbusFC == PacketBuilder::FC5){
            $answer = $modbsResponseHex == $sendDataHex ? true : false;        
        }
        
        if($modbusFC == PacketBuilder::FC16 or $modbusFC == PacketBuilder::FC15){
            $sendCountReg = $sendDataHex[8].$sendDataHex[9];
            $writeCountReg = $modbsResponseHex[8].$modbsResponseHex[9];
            $answer = $sendCountReg == $writeCountReg ? true : false;        
        }
        
        $this->answer = $answer;
    }
    
    private function parsAIO() 
    {
        $data = str_split(bin2hex(substr($this->modbusResponse, 9)), 4);
        $portType = $this->packetInfo->getProperty("dataType");
        foreach ($portType as $key => $type) {    
            
            switch ($type) {
                case $this->packetInfo::FLOAT:
                    $this->answer[$key] = $this->hexToFloat($data[$key+1].$data[$key]);
                    break;
                case $this->packetInfo::DOUBLE:                    
                    for ($i = 1; $i <= 4; $i++){                        
                        $hexDoubleArray[] = array_shift($data);
                    } 
                    
                    $this->answer[$key] = $this->hexToDouble($hexDoubleArray[3].$hexDoubleArray[2].$hexDoubleArray[1].$hexDoubleArray[0]);                    
                    $hexDoubleArray = [];
                    break;
                default:
                    $this->answer[$key] = hexdec($data[$key]);
                    break;
            }            
        }        
    }
    
    private function parsDIO() 
    {       
        $countByte = bin2hex(substr($this->modbusResponse, 8,1));
        $data = bin2hex(substr($this->modbusResponse, 9, hexdec($countByte)));        
        $regData = '';
        
        foreach (str_split($data,2) as $value) {
            $byte = base_convert($value, 16, 2);
            $completeByte = str_pad($byte, 8, "0", STR_PAD_LEFT);
            $regData .= strrev($completeByte);            
        }
        
        $startReg = hexdec($this->packetInfo->getProperty("startReg"));
        $countReg = hexdec($this->packetInfo->getProperty("countReg"));
        
        foreach(str_split($regData) as $key => $value){
            if($key >= $countReg){
                break;
            }
            
            $this->answer[$startReg] = $value;
            $startReg++;
        }        
    }
    
    private function hexToFloat($value) 
    {
        $bitData = base_convert($value, 16, 2);
        $bitComplete = str_pad($bitData, 32, "0", STR_PAD_LEFT);        
        $sign = pow(-1, (base_convert(substr($bitComplete, 0 ,1), 2, 10))); 
        $exponent = pow(2,(base_convert(substr($bitComplete, 1 ,8), 2, 10)-127)); 
        $mantissa = base_convert(substr($bitComplete, 9), 2, 10)/pow(2, 23)+1;
        
        return round($sign*$exponent*$mantissa, 2);
    }
    
    private function hexToDouble($value) 
    {        
        $bitData = base_convert($value, 16, 2);
        $bitComplete = str_pad($bitData, 64, "0", STR_PAD_LEFT);          
        $sign = pow(-1, (base_convert(substr($bitComplete, 0 ,1), 2, 10))); 
        $exponent = pow(2,(base_convert(substr($bitComplete, 1 ,11), 2, 10)-1023)); 
        $mantissa = base_convert(substr($bitComplete, 12), 2, 10)/pow(2, 52)+1;
        
        return round($sign*$exponent*$mantissa, 2);
    }
}


