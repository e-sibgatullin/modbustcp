<?php
namespace Modbus;

use Modbus\Interfaces\PacketBuilderInterface;
use Modbus\Exceptions\ModbusExceptions;

class PacketBuilder implements PacketBuilderInterface
{
    CONST FC1 = "01";              //Read Coil Status
    CONST FC2 = "02";              //Read Discrete Inputs
    CONST FC3 = "03";              //Read Holding Registers
    CONST FC4 = "04";              //Read Input Registers
    CONST FC5 = "05";              //Write Single Coil
    CONST FC6 = "06";              //Write Single Register   
    CONST FC15 = "0F";            //Write multi Coil
    CONST FC16 = "10";            //Write multi Register
    CONST COILON = 'FF00';       //Coil Status ON 
    CONST COILOFF = '0000';      //Coil Status OFF    
    CONST DEFAULT_COUNT_REG = 1;        
    CONST WORD = 1;
    CONST FLOAT = 2;
    CONST DOUBLE = 4;
    
    private $packet;

    public function create(int $protId = 0, int $devAddr = 1)
    {
        $this->resetPacket();
        $this->packet->idTransaction = $this->completeHex(rand(0, 65000), 4);
        $this->packet->idProtocol = $this->completeHex($protId, 4);
        $this->packet->devAddr = $this->completeHex($devAddr, 2);
        
        return $this;
    }
    
    public function fc(string $fc = self::FC3)
    {
        $this->packet->functionalCode = $fc;
        
        return $this;
    }
    
    public function dataTYpe(array $type) 
    {
        if(!in_array($this->packet->functionalCode, [self::FC3, self::FC4, self::FC6, self::FC16])){
            throw new ModbusExceptions("Для дискретных портов не нужно указывать тип данных.");            
        }       
        
        $this->packet->dataType = $type;
        $this->countReg(array_sum($type));
        
        return $this;
    }
    
    public function startReg(int $StartReg = 0)
    {
        $this->packet->startReg = $this->completeHex($StartReg, 4);
        
        return $this;
    }
    
    public function countReg(int $countReg = 1)
    {        
        $this->packet->countReg = $this->completeHex($countReg, 4);
        
        return $this;
    }
    
    public function valReg(int $val = 1)
    {
        if(!in_array($this->packet->functionalCode, [self::FC5, self::FC6])){
            throw new ModbusExceptions("Для данного вида операции используйте метод setCount");            
        }

        if($this->packet->functionalCode == self::FC5 and $val > 0){            
            $this->packet->regVal = self::COILON;            
        }
        
        if($this->packet->functionalCode == self::FC5 and $val <= 0){
            $this->packet->regVal = self::COILOFF;
        }
        
        if($this->packet->functionalCode == self::FC6){            
            $this->packet->regVal = $this->completeHex($val, 4);
        }
        
        return $this;
    }
    
    public function multiVal(array $MultiVal)
    {
        if(!in_array($this->packet->functionalCode, [self::FC15, self::FC16])){            
            throw new ModbusExceptions("Для данного вида операции не возможно использовать множественную вставку.");            
        }                        
        
        if($this->packet->functionalCode == self::FC16) {            
            $portType = $this->packet->dataType;
            foreach ($MultiVal as $key => $value) {                
                $type = array_shift($portType);                
                $hexArray[] = $this->completeHex($value, 4, $type);
            }
            $str = implode($hexArray);            
        }        
        
        if($this->packet->functionalCode == self::FC15) {
            $strArray = implode($MultiVal);
            $arrayData = str_split($strArray, 8);
            $str = implode(array_map(function($item) {
                        $finstr = str_pad($item, 8, '0', STR_PAD_RIGHT);                          
                        $dec = base_convert(strrev($finstr), 2, 10);
                        $hex = $this->completeHex($dec, 2);                        
                        return $hex;
                    }, $arrayData));  
            $this->countReg(count($MultiVal));
        }        
        
        $this->packet->multiVal = $str;
        
        return $this;
    }   
    
    public function getPacket()
    {        
        $this->buildPack();        
        return hex2bin($this->packet->headPart.$this->packet->dataPart);        
    }
    
    public function getProperty(string $propertyName) 
    {
        if(!property_exists($this->packet, $propertyName)){            
            throw new ModbusExceptions("Данные по полю $propertyName отсутсвуют.");
        }
        
        return $this->packet->{$propertyName};
    }
    
    public function __call($method, $params) 
    {
        throw new ModbusExceptions("Данный метод $method отсутсвует.");
    }
    
    private function buildDataPart() 
    {        
        $devAddr            = $this->getValProperty("devAddr");
        $functionalCode     = $this->getValProperty("functionalCode", "fc");
        $startReg           = $this->getValProperty("startReg", "startReg");
        $countOrValReg      = in_array($this->packet->functionalCode, [self::FC5, self::FC6]) ? 
                $this->getValProperty("regVal", "valReg") : 
                $this->getValProperty("countReg", "countReg");        
        $dataPart = $devAddr.$functionalCode.$startReg.$countOrValReg;
        
        if(property_exists($this->packet, "multiVal")){
            $countByte      = strlen(hex2bin($this->packet->multiVal));
            $countByteHex   = $this->completeHex($countByte, 2);
            $dataPart      .= $countByteHex . $this->packet->multiVal;
        }
        
        $this->packet->dataPart = $dataPart;
    }
    
    private function buildPack() 
    {
        if(!property_exists($this->packet, "dataPart")){
            $this->buildDataPart();
        }
        
        $idTransaction      = $this->getValProperty("idTransaction");
        $idProtocol         = $this->getValProperty("idProtocol");        
        $binDataPart        = hex2bin($this->packet->dataPart);
        $msgLeng            = $this->completeHex(strlen($binDataPart), 4);
        $head               = $idTransaction.$idProtocol.$msgLeng;
        
        $this->packet->headPart = $head;
    }


    private function resetPacket() 
    {
        $this->packet = new \stdClass;
    }
    
    private function completeHex($data, $length, $type = self::WORD) 
    {        
        switch ($type) {
            case self::FLOAT:                
                $result = $this->convertToFloatOrDouble("f", $data);
                break;
            case self::DOUBLE:                
                $result = $this->convertToFloatOrDouble("d", $data);
                break;
            default:
                $result = str_pad(dechex($data), $length, '0', STR_PAD_LEFT);
                break;
        }
        
        return $result;
    } 
    
    private function convertToFloatOrDouble(string $type, string $data) 
    {
        $result = '';
        $hexStr = bin2hex(pack($type, $data));
        $arrayByte = str_split($hexStr,2);
        $highByte = array_filter(
                $arrayByte, 
                function($value, $key){return !($key % 2);}, 
                ARRAY_FILTER_USE_BOTH);
        $lowByte = array_filter(
                $arrayByte, 
                function($value, $key){return $key % 2;}, 
                ARRAY_FILTER_USE_BOTH);
                
        foreach ($highByte as $value) {
            $result .= array_shift($lowByte).$value;
        }        
        
        return $result;        
    }
    
    private function getValProperty($property, $defaultVal = "create") 
    {
        if(property_exists($this->packet, $property)){
            return $this->packet->{$property};
        }
        
        $this->$defaultVal();
        return $this->packet->{$property};
    }
}
