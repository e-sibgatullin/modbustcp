<?php
namespace Modbus;

use Modbus\Interfaces\ConnectionsInterface;
use Modbus\Interfaces\ParserInterface;
use Modbus\Interfaces\PacketBuilderInterface;

class ModbusTCP
{    
    protected $host;
    protected $port; 
    protected $devAddr;
    protected $connection;
    protected $parser;
    protected $packetBuilder;

    private $responsePacket;    

    public function __construct(array $setings, ParserInterface $pars, PacketBuilderInterface $packetBuilder) 
    {        
        $this->host = $setings['host'];
        $this->port = $setings['port']; 
        $this->devAddr = $setings['devAddr'];        
        $this->connection = TCPConnect::getConnect($this->host, $this->port);
        $this->parser = $pars;
        $this->packetBuilder = $packetBuilder;        
    }    
    
    public function send() 
    {     
        socket_set_block($this->connection);
        $writeResult = @socket_write($this->connection, $this->packetBuilder->getPacket());          
        if($writeResult == 0 or $writeResult === false){  
            $msg = mb_convert_encoding(socket_strerror(socket_last_error()), "UTF-8", "CP-1251");
            throw new Exceptions\ConnectExceptions("Сообщение не отправлено на контролер $this->host." . $msg);
        } 
        
        $this->responsePacket = @socket_read($this->connection, 2000);         
        if(false === $this->responsePacket or empty($this->responsePacket)) {   
            $msg = mb_convert_encoding(socket_strerror(socket_last_error()), "UTF-8", "CP-1251");
            throw new Exceptions\ConnectExceptions("Не получен ответ от контролера $this->host. " . $msg);
        } 
        
        return $this;
    }
    
    public function getResponse() 
    {        
        return $this->parser->run($this->responsePacket, $this->packetBuilder);            
    }
    
    public function __call($method, $params) 
    {        
        if(!method_exists($this->packetBuilder, $method)){
            throw new ModbusExceptions("Данный метод $method отсутсвует.");
        }        
        
        if($method == 'fc'){
            $this->packetBuilder->create(0, $this->devAddr);
        }        
        
        $this->packetBuilder->$method(array_shift($params));
        return $this->packetBuilder;
    }
    
    public function reconn() 
    {
        $this->closeConnect();
        TCPConnect::$reconn = true;
        $this->connection = TCPConnect::getConnect($this->host, $this->port);
    }
            
    private function closeConnect() 
    {
        if(!is_resource($this->connection)){
            return;
        }
        
        socket_set_option(
                $this->connection,
                SOL_SOCKET,
                SO_LINGER,
                ['l_onoff'=>1,'l_linger'=>0]
                );
        @socket_close($this->connection);
    }
    
    function __destruct() 
    {
        $this->closeConnect();
    }
}
