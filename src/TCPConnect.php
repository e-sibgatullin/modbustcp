<?php
namespace Modbus;

use Modbus\Exceptions\ConnectExceptions;
use Modbus\Interfaces\ConnectionsInterface;

class TCPConnect implements ConnectionsInterface 
{    
    private static $connection;
    private static $host;
    private static $port;
    
    static $reconn = false;


    protected function __construct() { }
    
    protected function __clone() { }
    
    public function __wakeup()
    {
        throw new ConnectExceptions("Cannot unserialize a singleton.");
    }
    
    public static function getConnect($host = '127.0.0.1', $port = '502') 
    {        
        if(empty(self::$connection) || self::$host != $host || self::$reconn){
            self::$host = $host;
            self::$port = $port;   
            self::connect();
        }       
        
        return self::$connection;
    }    
    
    private static function connect() 
    {        
        self::$connection = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP);  
        socket_set_nonblock(self::$connection);        
        socket_set_option(
                self::$connection, 
                SOL_SOCKET, 
                SO_RCVTIMEO, 
                array('sec' => 1, 'usec' => 0)
                );
        socket_set_option(
                self::$connection, 
                SOL_SOCKET, 
                SO_SNDTIMEO, 
                array('sec' => 1, 'usec' => 0)
                );
        @socket_connect(self::$connection, self::$host, self::$port);
        self::$reconn = false;
    }    
}
