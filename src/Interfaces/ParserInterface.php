<?php
namespace Modbus\Interfaces;

interface ParserInterface
{
    public function run(string $responseModbus, \Modbus\PacketBuilder $portType);
}