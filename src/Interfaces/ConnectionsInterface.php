<?php
namespace Modbus\Interfaces;

interface ConnectionsInterface
{
    public static function getConnect($host, $port);    
}
