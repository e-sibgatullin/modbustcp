<?php
namespace Modbus\Interfaces;

interface PacketBuilderInterface 
{    
    public function create(int $protId, int $devAddr);
    public function fc(string $fc);
    public function dataTYpe(array $dataType);
    public function startReg(int $StartReg);
    public function countReg(int $countReg);
    public function valReg(int $val);
    public function multiVal(array $MultiVal);    
    public function getPacket();
    public function getProperty(string $propertyName);
}